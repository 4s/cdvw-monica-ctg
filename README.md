
Cordova plugin wrapper for the Monica AN24
==========================================
This wrapper packages and builds the platform-independent Monica AN24
CTG device integration module (https://bitbucket.org/4s/monica-ctg-module)

Purpose
-------
Various web frameworks has different ways of including and starting 
native programs. This project separates the Monica CTG code from 
cordova specific code.

Installation
-------------
To add Bluetooth support to a Cordova-based application, issue the
following command in the application folder:
```
cordova plugin add https://bitbucket.org/4s/cdvw-monica-ctg.git
```

Dependency
----------
There does not exist an explicit dependency on the cordova adaptor(https://bitbucket.org/4s/cordova-adaptor/)
but any use of this project requires the cordova adapter to be 
installed also.
The interplay between the projects are described in the following
section.

### Function
During installation of this project 3 files are copied into the 
cordova infrastructure.

- header_name.txt
- module_conan_id.txt
- module_name.txt
 
 These files are read by the Cordova Adaptor plugin during the app 
 building stage and this causes the Monica CTG module library to be 
 pulled and linked into the app.

 Furthermore the cordova adapter starts the baseplate and modules
 like this one.

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0